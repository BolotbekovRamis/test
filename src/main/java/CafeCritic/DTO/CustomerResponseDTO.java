package CafeCritic.DTO;

import CafeCritic.Model.Customer;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
public class CustomerResponseDTO {
    private int id;
    private String fullname;
    private String email;

    public static CustomerResponseDTO from(Customer user) {
        return builder()
                .id(user.getId())
                .fullname(user.getUsername())
                .email(user.getEmail())
                .build();
    }
}
