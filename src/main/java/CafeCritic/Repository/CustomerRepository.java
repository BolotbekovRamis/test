package CafeCritic.Repository;

import CafeCritic.Model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    boolean existsByEmail(String email);

    Optional<Customer> findByEmail(String email);

    boolean existsByUsername(String username);

    Customer findById(int id);

    Customer findByUsername(String username);
}
