package CafeCritic.Controller;

import CafeCritic.Exception.CustomerAlreadyRegisteredException;
import CafeCritic.Model.CustomerRegisterForm;
import CafeCritic.Service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("/profile")
    public String pageCustomerProfile(Model model, Principal principal)
    {
        var user = customerService.getByEmail(principal.getName());
        model.addAttribute("dto", user);
        model.addAttribute("user_status", getUserStatus());
        return "profile";
    }

    @GetMapping("/register")
    public String pageRegisterCustomer(Model model) {
        model.addAttribute("user_status", getUserStatus());

        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new CustomerRegisterForm());
        }

        return "register";
    }

    @PostMapping("/register")
    public String registerPage(@Valid CustomerRegisterForm customerRequestDto,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", customerRequestDto);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/register";
        }
        else{
            if(customerService.existByUsername(customerRequestDto)){
                throw new CustomerAlreadyRegisteredException();
            }
            customerService.register(customerRequestDto);
            return "redirect:/login";
        }
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        model.addAttribute("user_status", getUserStatus());
        return "login";
    }

    private String getUserStatus(){
        String user_status;
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        try{
            user_status = (String)authentication.getPrincipal();
        }
        catch (Exception ex){
            user_status = "authorizedUser";
        }
        return user_status;
    }
}